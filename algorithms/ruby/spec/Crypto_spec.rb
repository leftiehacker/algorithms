require 'rspec'
require_relative '../Crypto'

describe Crypto do
	describe "#hash(unhash)" do
		before {
			@hash = Crypto.new("acdekilmnoprstuy", 1693941520599974437)
			@hashVal = @hash.hash("indicator")
		}

		it "should invert the hash function" do
			expect(@hashVal).to eq @hash.hash(@hash.unhash)
		end
	end

	describe "#unhash" do
		before {
			@hash = Crypto.new("acdekilmnoprstuy", 1693941520599974437)
			@hashVal = @hash.hash("indicator")
			@word = @hash.unhash
		}

		it "should return the original word" do
			expect(@word).to eq "indicator"
		end
	end

	describe "#hash" do
		before {
			@hash = Crypto.new("acdekilmnoprstuy", 1693941520599974437)
			@hashVal = @hash.hash("indicator")
		}

		it "should return the hashed value" do
			expect(@hashVal).to eq @hash.get_hashVal
		end
	end
end