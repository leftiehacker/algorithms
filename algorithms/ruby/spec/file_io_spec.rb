require 'rspec'
require_relative '../file_io'

describe FileIO do
	describe "#readFile" do
		before { 
			fileio = FileIO.new('data.txt')
			@output = fileio.readFile
		}
		
		it "returns array of 0 to 10" do
			expect(@output).to eq Array.new(11) { |i| i}
		end
	end
end