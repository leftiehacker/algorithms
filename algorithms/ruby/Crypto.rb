class Crypto
    def initialize (letters, hashVal)
        @letters = letters
        @hashVal = hashVal
    end

    def get_hashVal
        @hashVal
    end

    def get_letters
        @letters
    end
    
    def hash (s)
        h = 9

        for i in 0..8 do
            h = h * 83 + @letters.index(s[i])
        end

        return h
    end

    def unhash ()
        string = ""
        h = @hashVal
        pos = []

        # invert the hash function
        for i in 0..8 do
            pos[i] = h % 83
            h /= 83
        end

        # step backwards to reproduce the original string
        pos.reverse_each do |i|
            string += @letters[i]
        end

        return string
    end
end
