/*
Given an array of integers such as [2,8,4,2,7,9,5,3,1], sort the array so that
all numbers located at even indices are smaller than all numbers at odd indices.
*/

const sortOddGreaterThanEven = function sortOddGreaterThanEven (array) {
  if (array.length < 2) return array;
  if (array.length === 2 && array[0] < array[1]) return array;

  const arr = array.sort();
  const left = arr.slice(0, arr.length / 2);
  const right = arr.slice(arr.length / 2);
  const merged = [];

  while (merged.length < array.length) {
    if (left.length) {
      const leftNode = left.shift();
      merged.push(leftNode);
    }
    if (right.length) {
      const rightNode = right.shift();
      merged.push(rightNode);
    }
  }

  return merged;
}
