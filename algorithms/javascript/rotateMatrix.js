function print(matrix) {
  matrix.forEach(row => {
    console.log(...row);
  });
}

function rotate90(matrix) {
  const rowLength = matrix[0].length;
  let i = 0, j = 0;

  while (i < rowLength) {
    j = i;
    while (j < rowLength) {
      if (i !== j) {
        [matrix[i][j], matrix[j][i]] =
        [matrix[j][i], matrix[i][j]];
      }
      j += 1;
    }
    i += 1;
  }

  return matrix.reverse();
}
