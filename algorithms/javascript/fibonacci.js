function fib(p, arr) {
  const n = arr.length
  if (n >= p) return arr

  arr[n] = arr[n - 1] + arr[n - 2]

  return fib(p, arr)
}

console.log(fib(10, [0,1]))
