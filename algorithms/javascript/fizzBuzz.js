function fizzBuzz(i, dictionary) {
  if (i === 100) return;
  let string = '';

  Object.keys(dictionary).forEach(divisor => {
    if (i % divisor === 0) {
      string += dictionary[divisor];
    }
  });

  console.log(string || i);
  fizzBuzz(i++, dictionary)
}

fizzBuzz(1, {
  3: 'Fizz',
  5: 'Buzz',
});
