class Perceptron {
  constructor(c) {
    this.c = c
    this.weights = {
      a: Math.rand(-1,1), 
      b: Math.rand(-1,1)
    }
  }

  static activate(sum) { 
    return sum > 0 ? 1 : -1 
  }

  feedForward(input) { 
    const sum = this.weight.a * input.x + this.weight.b * input.y 
    return activate(sum)
  }

  train(input, expected) {
    const output = feedForward(input)
    const error = expected - output
    this.weight.a = rate * error * input.x
    this.weight.b = rate * error * input.y
  }
}

class Trainer {
  constructor(x, y) {
    this.inputs = {x, y}
    this.expected = this.expect()
  }

  f(x) { 
    return 4 * x - 2
  }

  expect() {
    const y = this.f(this.inputs.x)
    return this.inputs.y > y ? 1 : -1
  }
}

const ptron = new Perceptron(0.1)
const t = new Trainer(4, 6)

ptron.train(t.inputs, t.expected)



