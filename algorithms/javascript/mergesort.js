function mergesort(a) {
  if (a.length < 2) return a

  const mid = Math.round(a.length/2)
  const left = a.slice(0, mid)
  const right = a.slice(mid)

  return merge(
    mergesort(left),
    mergesort(right)
  )
}

function merge(left, right) {
  const sorted = [];

  while (left.length > 0 && right.length > 0) {
    if (left[0] <= right[0]) {
      sorted.push(left.shift())
    } else {
      sorted.push(right.shift())
    }
  }

  return sorted.concat(left, right)
}

const arr = [2,3,6,8,2,1,8,32,63,1,6]

console.log(mergesort(arr))
