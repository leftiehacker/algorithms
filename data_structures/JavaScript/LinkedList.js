class LinkedList {
  constructor() {
    this.first = null
  }
}

//add node to end
LinkedList.prototype.Add = val => {
  let node = {
    value: val,
    next: null
  }
  if (!this.first) {
     this.first = node
  } else {
     let curr = this.first
     while (curr.next) {
       curr = curr.next
     }
     curr.next = node
  }
}

LinkedList.prototype.Print = () => {
  let currentNode = this.head
  if (!currentNode) return

  let list = ''

  while (currentNode) {
    list += currentNode.value

    if (!currentNode.next) break

    list += ' -> '
    currentNode = currentNode.next
  }

  console.log(list)
}

//remove node from end
LinkedList.prototype.pop = () => {
  let val = this.first
  this.first = this.first.next
  return val
}

//remove node at index
LinkedList.prototype.delete = index => {
  let i = 0, curr = this.first, prev
  for (; i < index; i++) {
    prev = curr
    curr = curr.next
  }
  if (index === 0) {
    this.first = this.first.next
  }
  prev.next = curr.next
  return curr.val
}

//binary search for value
LinkedList.prototype.find = val => {
  let i = 0, curr = this.first
  while (curr) {
    if (curr.value === val) {
      return i
    }
    curr = curr.next
    i++
  }
  return -1
}

//transform LinkedList to Array
LinkedList.prototype.toArray = () => {
  let arr = [], curr = this.first
  while (curr) {
    arr.push(curr.value)
    curr = curr.next
  }
  return arr
}
