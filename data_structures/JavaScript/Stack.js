var Stack = function () {
    this.length = 0
    this.contents = {}
    
    this.push = function (element) {
        this.contents[this.length] = element
        this.length++
    }

    this.pop = function () {
        if (this.length === 0) return undefined
        this.length--
        var el = this.contents[this.length]
        delete this.contents[this.length]
        return el
    }

    this.size = function () {
        return this.length
    }

    this.top = function () {
        return this.contents[this.length - 1]
    }
}
