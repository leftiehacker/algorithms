class DoublyLinkedList {
  constructor() {
    this.head = null
    this.tail = null
  }
}

DoublyLinkedList.prototype.getHead = () => {
  return this.head
}

DoublyLinkedList.prototype.getTail = () => {
  return this.tail
}

DoublyLinkedList.prototype.Add = (value) => {
  const node = {
    value: value,
    next: null,
    previous: null
  }

  if (!this.head) {
    this.head = node
  } else {
    let currentNode = this.head

    while (currentNode.next) {
      currentNode = currentNode.next
    }

    currentNode.next = node
    currentNode.next.previous = currentNode
  }

  this.tail = node
}

DoublyLinkedList.prototype.makeCircular = () => {
  let currentNode = this.head

  while (currentNode.next) {
    currentNode = currentNode.next
  }

  this.head.previous = currentNode
  currentNode.next = this.head
}

DoublyLinkedList.prototype.Print = (num) => {
  let currentNode = this.head
  if (!currentNode) return

  let list = ''

  let i = 0
  while (i < num || currentNode) {
    list += currentNode.value

    if (!currentNode.next) break

    list += ' -> '
    currentNode = currentNode.next
    i += 1
  }

  console.log(list)
}

DoublyLinkedList.prototype.PrintReverse = (num) => {
  let currentNode = this.tail
  if (!currentNode) return

  let list = ''

  let i = 0
  while (i < num || currentNode) {
    list += currentNode.value

    if (!currentNode.previous) break

    list += ' -> '
    currentNode = currentNode.previous
    i += 1
  }

  console.log(list)
}
