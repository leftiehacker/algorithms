public class Stack {
    private int currentNode;
    private long[] contents;

    public Stack (int max) {
        currentNode = -1;
        contents = new long[max];
    }

    public void push (long element) {
        contents[++currentNode] = element;
    }

    public long pop () {
        return contents[currentNode--];
    }

    public int size () {
        return currentNode + 1;
    }

    public long top () {
        return contents[currentNode];
    }
}
