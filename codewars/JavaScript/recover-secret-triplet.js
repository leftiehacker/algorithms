/*
Description:

There is a secret string which is unknown to you. Given a collection of random triplets from the string, recover the original string.
A triplet here is defined as a sequence of three letters such that each letter occurs somewhere before the next in the given string. 
"whi" is a triplet for the string "whatisup". As a simplification, you may assume that no letter occurs more than once 
in the secret string.

You can assume nothing about the triplets given to you other than that they are valid triplets and that they contain 
sufficient information to deduce the original string. In particular, this means that the secret string will never contain 
letters that do not occur in one of the triplets given to you.
*/

function recoverSecret(triplets) {
  var edges = [],
  secret = ''

  for(var el of triplets) {
    var edge1 = el[0] + el[1]
    edge2 = el[1] + el[2]
    if (edges.indexOf(edge1) === -1) edges.push(edge1)
    if(edges.indexOf(edge2) === -1) edges.push(edge2)
  }

  var next = visit(edges)
  while (next) {
    secret += next
    edges = edges.filter(edge => edge.indexOf(next) === -1)
    next = visit(edges)
  }

  return secret
}

function visit(edges) {
  var next = edges.find(edge1 => edges.every(edge2 => edge1[0] !== edge2[1]))
  return edges.length > 1 ? next[0] : edges[0]
}
