function nextBigger(n) {
  let nArr = n.toString().split('');
  for (let i = nArr.length-1; i > 0; i--) {
    if (nArr[i] > nArr[i-1]) {
      let right = nArr.splice(i-1),
      first = right.splice(0, 1).shift(),
      next = {el:null, i:null}
      right.forEach((el, i) => {
        if (el > first && (next.el == null || next.el > el)) {
            next.el = el
            next.i = i
        }
      })
      right.splice(next.i, 1)
      right.push(first)
      return +nArr.concat(next.el).concat(right.sort()).join('')
    }
  }
  return -1
}

/*
You have to create a function that takes a positive integer number and returns 
the next bigger number formed by the same digits:

next_bigger(12)==21
next_bigger(513)==531
next_bigger(2017)==2071

If no bigger number can be composed using those digits, return -1:

next_bigger(9)==-1
next_bigger(111)==-1
next_bigger(531)==-1
*/