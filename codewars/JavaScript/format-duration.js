/*
Description:

Your task in order to complete this Kata is to write a function which formats a duration,
given as a number of seconds, in a human-friendly way.

The function must accept a non-negative integer. If it is zero, it just returns "now".
Otherwise, the duration is expressed as a combination of years, days, hours, minutes and seconds.

It is much easier to understand with an example:
  formatDuration(62)    // returns "1 minute and 2 seconds"
  formatDuration(3662)  // returns "1 hour, 1 minute and 2 seconds"

Note that spaces are important.

Detailed rules:

The resulting expression is made of components like 4 seconds, 1 year, etc. In general,
a positive integer and one of the valid units of time, separated by a space. The unit of time is used
in plural if the integer is greater than 1.

The components are separated by a comma and a space (", "). Except the last component,
which is separated by " and ", just like it would be written in English.

A more significant units of time will occur before than a least significant one.
Therefore, 1 second and 1 year is not correct, but 1 year and 1 second is.

Different components have different unit of times. So there is not repeated units like in 5 seconds and 1 second.

A component will not appear at all if its value happens to be zero. Hence, 1 minute and 0 seconds is not valid,
but it should be just 1 minute.

A unit of time must be used "as much as possible". It means that the function should not return 61 seconds,
but 1 minute and 1 second instead. Formally, the duration specified by of a component must not be greater than any valid more significant unit of time.

For the purpose of this Kata, a year is 365 days and a day is 24 hours.
*/

function formatDuration (seconds) {
  if (seconds == 0) return 'now'

  var str = '',
  arr = [],
  rem

  if (seconds >= 31536000) {
    rem = seconds % 31536000
    arr.push(
      seconds / 31536000 + ' year',
      rem / 86400 + ' day',
      (rem %= 86400) / 3600 + ' hour',
      (rem %= 3600) / 60 + ' minute',
      (rem %= 60) + ' second'
    )
  } else if (seconds >= 86400) {
    rem = seconds % 86400
    arr.push(
      seconds / 86400 + ' day',
      rem / 3600 + ' hour',
      (rem %= 3600) / 60 + ' minute',
      (rem %= 60) + ' second'
    )
  } else if (seconds >= 3600) {
    rem = seconds % 3600
    arr.push(
      seconds / 3600 + ' hour',
      rem / 60 + ' minute',
      (rem %= 60) + ' second'
    )
  } else if (seconds >= 60) {
    rem = seconds % 60
    arr.push(
      seconds / 60 + ' minute',
      rem + ' second'
    )
  } else {
    arr.push(seconds + ' second')
  }

  arr = arr.filter(el => el.charAt(0) > 0)

  arr.forEach((el, i) => {
    el = el.split(' ')
    el[0] = Math.floor(el[0])

    str += (el[0] > 1)
      ? el.join(' ') + 's'
      : el.join(' ')

    str += (i == arr.length - 2)
      ? ' and '
      : (i < arr.length - 1)
      ? ', '
      : ''
  })

  return str
}
