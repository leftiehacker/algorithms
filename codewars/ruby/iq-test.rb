def iq_test(numbers)
  nums = numbers.split.map{ |num| num.to_i.even? }
  return nums.count(true) == 1 ?
         nums.index(true) + 1 :
         nums.index(false) + 1
end

# Bob is preparing to take an IQ test. The most frequent task in this test is to figure out which of the given numbers differs from the others. Bob observed that one number usually differs from the others in evenness. To help Bob check his answers, he needs a program that finds the number that is different in evenness, and return the position of this number.
#
# ! Keep in mind that your task is to help Bob solve a real IQ test, which means indexes of the elements start from 1 (not 0)
#
# ##Examples :
#
# iq_test("2 4 7 8 10") => 3 // Third number is odd, while the rest of the numbers are even
#
# iq_test("1 2 1 1") => 2 // Second number is even, while the rest of the numbers are odd
