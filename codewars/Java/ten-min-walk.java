public class TenMinWalk {
  public static boolean isValid(char[] walk) {
    if(walk.length != 10) return false;
    int sum = 0;
    for(int i = 0; i < 10; i++) {
      if(walk[i] == 'n' || walk[i] == 'e') {
        sum++;
      } else {
        sum--;
      }
    }
    return sum == 0;
  }
}
