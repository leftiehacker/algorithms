/*
Sum all the numbers of the array except the highest and the lowest element (the value, not the index!).
(The highest/lowest element is respectively only one element at each edge, even if there are more than 
one with the same value!)

Example:
{ 6, 2, 1, 8, 10 } => 16
{ 1, 1, 11, 2, 3 } => 6

If array is empty, null or None, or if only 1 Element exists, return 0.
*/

using System;
using System.Linq;

public static class Kata
{
  public static int Sum(int[] numbers)
  {
    if (numbers == null || !numbers.Any()) return 0;
    int max = Array.IndexOf(numbers, numbers.Max());
    int min = Array.IndexOf(numbers, numbers.Min());
    return numbers.Where((x,i) => i != max && i != min).Sum();
  }
}