/*
Write a function done_or_not/DoneOrNot passing a board (list[list_lines]) as parameter.
If the board is valid return 'Finished!', otherwise return 'Try again!'

Sudoku rules:
Complete the Sudoku puzzle so that each and every row, column,
and region contains the numbers one through nine only once.
*/

using System.Linq;

public class Sudoku
{
  public static string DoneOrNot(int[][] board)
  {
   for(int i = 0; i < board.Length; i++)
   {
     if (
       board[i].Sum() != 45 
       || board.Sum(x => x[i]) != 45
       || board.Skip(3 * (i / 3)).Take(3)
          .SelectMany(r => r.Skip(3 * (i % 3))
          .Take(3)).Sum() != 45
     ) return "Try again!";
   }
   return "Finished!";
  }
}