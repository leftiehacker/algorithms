'''
Given the  triangle of consecutive odd numbers:

             1
          3     5
       7     9    11
   13    15    17    19
21    23    25    27    29

Calculate the row sums of this triangle from the row index (starting at index 1), e.g.:

row_sum_odd_numbers(1); # 1
row_sum_odd_numbers(2); # 3 + 5 = 8
'''

def row_sum_odd_numbers(n):
    s = 0
    d = n * (n - 1) + 1 #first digit in nth row
    for x in range(0,n):
        s += d + x * 2
    return s

#alternatively, since the row sum is simply the row index cubed

def cube_row_index(n):
    return n**3
