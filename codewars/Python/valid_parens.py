# Write a function called that takes a string of parentheses, and determines if the 
# order of the parentheses is valid. The function should return true if the string is 
# valid, and false if it's invalid.

# Examples
# "()"              =>  true
# ")(()))"          =>  false
# "("               =>  false
# "(())((()())())"  =>  true

def valid_parentheses(string):
  p = 0

  for c in string:
    if c == '(': p += 1
    elif c == ')': p -= 1
    if p < 0: return False

  return p == 0