'''
You are given an array (which will have a length of at least 3, but could be very large) containing integers.
The array is either entirely comprised of odd integers or entirely comprised of even integers except for a single integer N.
Write a method that takes the array as an argument and returns N.
'''

def find_outlier(integers):
    first = integers[0]
    second = integers[1]
    third = integers[2]
    if first % 2 == second % 2:
        result = first
    elif first % 2 == third % 2:
        result = first
    elif second % 2 == third % 2:
        return first
    else:
        return second
    for num in integers:
        if num % 2 != result % 2:
            return num
    return None
