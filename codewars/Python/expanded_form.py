# You will be given a number and you will need to return it as a string in Expanded Form. For example:

# expandedForm(12); // Should return '10 + 2'
# expandedForm(42); // Should return '40 + 2'
# expandedForm(70304); // Should return '70000 + 300 + 4'

# NOTE: All numbers will be whole numbers greater than 0.

def expanded_form(num):
  list = []
  num = str(num)
    
  while len(num) > 0:
    expand = num[0]
    num = num[1:]
    while expand == '0':
      if len(num) <= 0: break
      expand = num[0]
      num = num[1:]
    for i in range(len(num)):
      expand += '0'
    if expand != '0':
      list.append(expand)
    
  return ' + '.join(list)