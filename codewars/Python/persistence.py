'''
Write a function, persistence, that takes in a positive parameter num and returns its multiplicative persistence,
which is the number of times you must multiply the digits in num until you reach a single digit.
'''

def persistence(n):
    c = 0
    while n > 9:
        s = 1
        for x in str(n):
            s *= int(x)
        c += 1
        n = s
    return c
