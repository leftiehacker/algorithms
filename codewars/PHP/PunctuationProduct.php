<?php

// Count the number of exclamation marks and question marks, return the product.

// Examples:
// Product("") == 0
// product("!") == 0
// Product("!ab? ?") == 2
// Product("!!") == 0
// Product("!??") == 2
// Product("!???") == 3
// Product("!!!??") == 6
// Product("!!!???") == 9
// Product("!???!!") == 9
// Product("!????!!!?") == 20

function product(string $s): int {
  $q = 0;
  $e = 0;
  for ($i = 0; $i < strlen($s); $i++) {
    if ($s[$i] == '?') {
      $q += 1;
    } elseif ($s[$i] == '!') {
      $e += 1;
    }
  }
  return $q * $e;
}

?>