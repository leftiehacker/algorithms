<?php

// Given a string of words, return the length of the shortest word(s).

function findShort($str){
  $split = explode(' ', $str);
  foreach ($split as $index => $word) {
    $split[$index] = strlen($word);
  }
  return min($split);
}

?>